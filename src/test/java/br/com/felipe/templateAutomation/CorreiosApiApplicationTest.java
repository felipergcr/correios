package br.com.felipe.templateAutomation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CorreiosApiApplicationTest {

	public static void main(String[] args) {
		SpringApplication.run(CorreiosApiApplicationTest.class, args).close();
	}

}