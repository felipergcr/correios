package br.com.felipe.templateAutomation.config;

import java.awt.Toolkit;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class BrowserFactory {

	public static WebDriver createChromeDriver() {
		
		ChromeOptions options = new ChromeOptions();
		
		options.addArguments("--disable-notifications");
		options.addArguments("--disable-extensions");
		options.addArguments("--disable-web-security");
		
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/test/resources/driver/windows/chromedriver.exe");

		return new ChromeDriver(options);
	}

	public static WebDriver createFirefoxDriver() {
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/src/test/resources/driver/windows/geckodriver.exe");
		return new FirefoxDriver();
	}

	public static WebDriver createIEDriver() {
		System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "/src/test/resources/driver/windows/IEDriverServer.exe");
		return new InternetExplorerDriver();
	}

	public static void addAllBrowserSetup(WebDriver driver) {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().setPosition(new Point(0, 0));
		java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension dim = new Dimension((int) screenSize.getWidth(), (int) screenSize.getHeight());
		driver.manage().window().setSize(dim);
	}

}
