package br.com.felipe.templateAutomation.config;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.felipe.templateAutomation.integrado.report.LogReport;

public class Webdriver {
	
	private static  WebDriver driver;
	private int TIMEOUT = 30;

	public Webdriver(WebDriver driver) {
		BrowserFactory.addAllBrowserSetup(driver);
		PageFactory.initElements(driver, this);
	}

	public Webdriver() {
		
		Property.loadProperties();
		Browsers.browserForName(Property.BROWSER);

		if (Property.BROWSER.toString().equals(Browsers.CHROME.name()))
			driver = BrowserFactory.createChromeDriver();

		if (Property.BROWSER.toString().equals(Browsers.FIREFOX.name()))
			driver = BrowserFactory.createFirefoxDriver();

		if (Property.BROWSER.toString().equals(Browsers.IE.name()))
			driver = BrowserFactory.createIEDriver();
	}
	
	public void refresh() {
		
		driver.navigate().refresh();
	}

	public void navegateTo(String url) {
		driver.navigate().to(url);
	}
	
	public void reload(By elementBy) {

		final WebElement htmlRoot = driver.findElement(By.tagName("html"));

		final long startTime = System.currentTimeMillis();
	    final long maxLoadTime = TimeUnit.SECONDS.toMillis(3000L);
	    boolean startedReloading = false;
	    
	    do {
	        try {
	        	
	        	startedReloading = !htmlRoot.isDisplayed();
	            
	        } catch (ElementNotVisibleException | StaleElementReferenceException ex) {

	        	startedReloading = true;
	        }
	        
	    } while (startedReloading && (System.currentTimeMillis() - startTime < maxLoadTime));

	    
	    waitVisibility(elementBy);
	}

	public static WebDriver getDriver() {
		return driver;
	}

	public static void closeNavegator() {
		try {
			getDriver().quit();
			driver = null;
		} catch (UnreachableBrowserException e) {
			LogReport.extentFail("ERRO AO FECHAR NAVEGADOR: " + e.getMessage());
		}
	}

	private static class BrowserCleanup implements Runnable {
		public void run() {
			closeNavegator();
		}
	}

	public static void loadPage(String url) {
		getDriver();
		getDriver().get(url);
	}

	public static void reopenAndLoadPage(String url) {
		driver = null;
		getDriver();
		loadPage(url);
	}

	public void waitVisibility(By elementBy) {
		try {
			
			WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
		    wait.until(ExpectedConditions.visibilityOf(driver.findElement(elementBy)));
		    
		} catch (Exception e) {
			LogReport.fail("ELEMENTO NÃO ESTA VISIVEL: " + elementBy);
		}
	}
	
	public void trocarAba(int index) {
		List<String> abas = new ArrayList<>(driver.getWindowHandles());
		
		driver.switchTo().window(abas.get(index));
	}
	
	public Boolean ElementIsPresent(By elementBy) {
		
	    boolean isPresent = true;
	    
		try {
			
			WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
			wait.until(ExpectedConditions.presenceOfElementLocated(elementBy));
			isPresent = true;
			
		} catch (Exception ex) {
		
			isPresent = false;
		}
		
		return isPresent;
	}
	
	public void waitPresenceOfElementLocate(By elementBy) {
		try {
			
			WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
			wait.until(ExpectedConditions.presenceOfElementLocated(elementBy));
			
		} catch (Exception e) {
			LogReport.fail("ELEMENTO NÃO ESTA PRESENTE NO DOOM : " + elementBy);
		}
	}
	
	public void visibilityOfElementLocated(By elementBy) {
		try {
			
			WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
			wait.until(ExpectedConditions.visibilityOfElementLocated(elementBy));
			
		} catch (Exception e) {
			LogReport.fail("ELEMENTO NÃO VISIVEL NE TELA E NEM PRESENTE NO DOOM : " + elementBy);
		}
	}
	
	public void clear(By elementBy) {
		waitVisibility(elementBy);
		driver.findElement(elementBy).clear();
	}

	public void click(By elementBy) {
		waitVisibility(elementBy);
		driver.findElement(elementBy).click();
	}

	public void sendKeys(By elementBy, String text) {
		waitPresenceOfElementLocate(elementBy);
		driver.findElement(elementBy).sendKeys(text);
	}

	public String getText(By elementBy) {
		visibilityOfElementLocated(elementBy);
		return driver.findElement(elementBy).getText();
	}

	public void assertEquals(By elementBy, String expectedText) {
		waitVisibility(elementBy);
		Assert.assertEquals(getText(elementBy), expectedText);
	}
	
	public String getAttributeValue(By elementBy, String attribute) {
		waitVisibility(elementBy);
		return driver.findElement(elementBy).getAttribute(attribute);
	}
	
	public Integer getIndiceElement(By ElementBy, String cnpj) {

		List<WebElement> elements = driver.findElements(ElementBy);

		Integer indice = 0;
		Integer indiceTrue = 0;

		for (WebElement element : elements) {

			if (element.getText()
					.replace(".", "")
					.replace("-", "")
					.replace("/", "").equals(cnpj)) {

				indiceTrue = indice - 1;
			}
			indice += 1; 
		}

		return indiceTrue;
	}
	
	public void fecharAba() {
		
		 driver.close();
		 trocarAba(0);
	}
	
}
