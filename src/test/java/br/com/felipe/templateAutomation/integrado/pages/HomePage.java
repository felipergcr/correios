package br.com.felipe.templateAutomation.integrado.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import br.com.felipe.templateAutomation.config.Webdriver;
import br.com.felipe.templateAutomation.integrado.report.LogReport;
import br.com.felipe.templateAutomation.utils.Mensagens;

public class HomePage extends Webdriver {

	// *********Web Elements*********

	private By HOME_BANNER = By.id("content-banner");
	private By INPUT_CEP_LOGRADOURO = By.id("acesso-busca");
	private By BUTTON_SEARCH = By.xpath("//*[@id='conteudo-inicial']/div/div[2]/div/form[2]/div[2]/button");
	private By MENSAGE = By.xpath("//*[@class='ctrlcontent']/p");
	private By TABELA_DADOS = By.xpath("//*[@class='tmptabela']/tbody/tr[2]/td");
	private By SAIDA_TABELA_DADOS = By.xpath("//*[@class='tmptabela']/tbody/tr");

	public HomePage(WebDriver driver) {
		super(driver);
	}

	public void aguardarBanner() {
		waitVisibility(HOME_BANNER);
	}
	
	public void inserirCepOuLogradouro(String dado) {
		LogReport.info(new StringBuilder("METODO DE REALIZAR BUSCA POR CEP OU LOGRADOURO: ").append(dado).toString());
		clear(INPUT_CEP_LOGRADOURO);
		sendKeys(INPUT_CEP_LOGRADOURO, dado);
		click(BUTTON_SEARCH);
	}
	
	public void validarMensagemBusca() {
		
		waitVisibility(MENSAGE);
		
		WebElement element = getDriver().findElement(MENSAGE);
		
		LogReport.passFail(element.getText().contains(Mensagens.DADOS_ENCONTRADOS_COM_SUCESSO), 
				new StringBuilder("VALIDACAO DE MENSAGEM ESPERADA: ").append(Mensagens.DADOS_ENCONTRADOS_COM_SUCESSO).append("</br> MENSAGEM RETORNA: ").append(element.getText()).toString());
	}
	
	public void validarPesquisa(String logradouro, String bairro, String localidade, String cep) {
		
		validarMensagemBusca();
		
		List<WebElement> elements = getDriver().findElements(TABELA_DADOS);

		Boolean achouLogradouro = elements.stream().filter((element) -> element.getText().contains(logradouro)).findFirst().isPresent();
		
		Boolean achouBairro = elements.stream().filter((element) -> element.getText().contains(bairro)).findFirst().isPresent();
		
		Boolean achouLocalidade = elements.stream().filter((element) -> element.getText().contains(localidade)).findFirst().isPresent();
		
		Boolean achouCep= elements.stream().filter((element) -> element.getText().contains(cep)).findFirst().isPresent();
		
		LogReport.passFail(achouLogradouro, 
				new StringBuilder("VALIDACAO DE LOGRADOURO ESTA CORRETA: ").append(achouLogradouro).toString());
		LogReport.passFail(achouBairro, 
				new StringBuilder("VALIDACAO DE BAIRRO ESTA CORRETA: ").append(achouBairro).toString());
		LogReport.passFail(achouLocalidade, 
				new StringBuilder("VALIDACAO DE LOCALIDADE ESTA CORRETA: ").append(achouLocalidade).toString());
		LogReport.passFail(achouCep, 
				new StringBuilder("VALIDACAO DE CEP ESTA CORRETA: ").append(achouCep).toString());
		
		saidaPesquisa();
	}
	
	public void saidaPesquisa() {
				
		validarMensagemBusca();
		
		List<WebElement> elements = getDriver().findElements(SAIDA_TABELA_DADOS);
		
		for (WebElement element : elements) {
			
			LogReport.info(new StringBuilder("SAIDA: ").append(element.getText()).toString());
		}		
	}

}
