package br.com.felipe.templateAutomation.integrado.report;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.junit.runner.Description;

import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.KlovReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.mongodb.MongoClientURI;

import br.com.felipe.templateAutomation.config.Property;

public class BaseReport {
	
	private static ExtentReports extentReports = null;
	private static KlovReporter klovReporter = null;
	private static File report;
	private static Map<Integer, ExtentTest> extentTestMap = new HashMap<Integer, ExtentTest>();

	public static ExtentReports createReporter() {

		Logger mongoLogger = Logger.getLogger("org.mongodb.driver");
		mongoLogger.setLevel(Level.SEVERE);

		try {
			report = new File("REPORT-CORREIOS.html");
			report.createNewFile();
		} catch (IOException e1) {
			Log.alerta("Nao foi possvel criar o arquivo report.html");
		}

		ExtentHtmlReporter extentHtmlReporter = new ExtentHtmlReporter("REPORT-CORREIOS.html");
		extentHtmlReporter.config().setTheme(Theme.DARK);
		extentHtmlReporter.config().setEncoding("UTF-8");
		extentHtmlReporter.config().setDocumentTitle(Property.EXREPORTS_DOC_TITLE);
		extentHtmlReporter.config().setReportName(Property.EXREPORTS_REPORT_NAME);
		extentHtmlReporter.config().setChartVisibilityOnOpen(true);
		extentHtmlReporter.setAnalysisStrategy(AnalysisStrategy.SUITE);

		klovReporter = new KlovReporter("Teste api", "Teste Api");
		configMongoConnection();

		extentReports = new ExtentReports();
		extentReports.attachReporter(extentHtmlReporter, klovReporter);

		try {
			extentReports.setSystemInfo("HostName", InetAddress.getLocalHost().getHostName());
			extentReports.setSystemInfo("IP Address", InetAddress.getLocalHost().getHostAddress());
			extentReports.setSystemInfo("OS", System.getProperty("os.name"));
			extentReports.setSystemInfo("UserName", System.getProperty("user.name"));
			extentReports.setSystemInfo("Java Version", System.getProperty("java.version"));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		return extentReports;
	}

	public static ExtentReports getExtentReports() {
		return extentReports;
	}

	public static void saveReporter() {
		if (extentReports != null) {
			extentReports.flush();
			moveReport();
		}
	}

	public static synchronized ExtentTest createTest(Description description) {
		
		ExtentTest extentTest = extentReports.createTest(description.getTestClass().getSimpleName())
				.createNode(description.getMethodName());
		extentTestMap.put((int) (Thread.currentThread().getId()), extentTest);
		return extentTest;
	}

	public static synchronized ExtentTest createTest(String testname, String testdesc) {
		ExtentTest extentTest = extentReports.createTest(testname, testdesc);
		extentTestMap.put((int) (Thread.currentThread().getId()), extentTest);
		return extentTest;
	}

	public static synchronized ExtentTest fetchTest() {
		return extentTestMap.get((int) (Thread.currentThread().getId()));
	}

	public static String retornarDataAtual() {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH");
		Calendar calendar = Calendar.getInstance();
		return format.format(calendar.getTime());
	}

	private static void conectMongo(MongoClientURI uri) {

		klovReporter.initMongoDbConnection(uri);
		klovReporter.setKlovUrl(Property.KLOV_URL);
		Log.info("Extent Reports conectou-se ao MongoDB corretamente.");
	}

	public static boolean configMongoConnection() {

		boolean conected = false;
		String mongoURI = Property.MONGO_URI;
		MongoClientURI uri = null;

		try {
			if (mongoURI != null) {
				uri = new MongoClientURI(mongoURI);
				Log.info("Iniciando conexão com o MongoDB via URI - " + uri);
				conectMongo(uri);
				return true;
			} else {
				Log.alerta("IP e Porta para conexão ao MongoDB nao foram inseridos");
			}
		} catch (Exception e) {
			Log.alerta("Extent Reports não se conectou ao mongoDB");
		}
		return conected;
	}

	public static void moveReport() {
		try {
			
			FileUtils.copyFile(new File("REPORT-CORREIOS.html"),
					new File("src/test/resources/reports/" + Property.EMISSOR + "/" + BaseReport.retornarDataAtual()
							+ "/report-" + Property.EMISSOR + "-" + BaseReport.retornarDataAtual() + ".html"));
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
