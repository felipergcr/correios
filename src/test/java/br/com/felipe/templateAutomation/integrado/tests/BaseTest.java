package br.com.felipe.templateAutomation.integrado.tests;

import java.util.Objects;

import org.apache.log4j.PropertyConfigurator;
import org.junit.AfterClass;
import org.junit.AssumptionViolatedException;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.felipe.templateAutomation.CorreiosApiApplicationTest;
import br.com.felipe.templateAutomation.config.Property;
import br.com.felipe.templateAutomation.integrado.report.BaseReport;
import br.com.felipe.templateAutomation.integrado.report.Log;
import br.com.felipe.templateAutomation.integrado.report.LogError;
import br.com.felipe.templateAutomation.integrado.report.LogReport;
import br.com.felipe.templateAutomation.utils.LimparDiretorio;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CorreiosApiApplicationTest.class)
@TestPropertySource(locations = "classpath:application.yml")
public class BaseTest {

	protected static Property property;

	public static boolean isUsingExtent = true;

	@BeforeClass
	public static void loadConfiguration() {

		PropertyConfigurator.configure(System.getProperty("user.dir") + "/src/test/resources/log4j.properties");
		property.loadProperties();

		try {
			if (BaseReport.getExtentReports() == null) {
				BaseReport.createReporter();
			}
		} catch (Exception e) {

			Log.alerta("ExtentReports nao se conectou ao MongoDB");
			Log.alerta("Logs esto sendo gerados apenas no automacao.log");
			isUsingExtent = false;
		}
	}

	@AfterClass
	public static void finalizar() {

		BaseReport.saveReporter();
		LimparDiretorio.limparDiretorio("/tmp");
	}

	@Rule
	public final TestRule testRule = new TestWatcher() {

		@Override
		protected void succeeded(Description description) {

			LogReport.pass("Teste realizado com sucesso!");
		}

		@Override
		protected void failed(Throwable e, Description description) {

			if (isUsingExtent) 
				LogReport.extentFail("Teste " + description.getMethodName() + " falhou");
			
			Log.erro(Objects.isNull(LogError.getErro()) 
					? "Erro: " + LogError.getErro() 
					: "Erro: " + e.getMessage());
			LogError.setErro(null);
		}

		@Override
		protected void skipped(AssumptionViolatedException e, Description description) {

			if (isUsingExtent) 
				LogReport.skip("Teste ignorado");
		}

		@Override
		protected void starting(Description description) {

			if (isUsingExtent) {
				BaseReport.createTest(description);
				LogReport.extentInfo("Emissor: " + Property.EMISSOR + ".<br/> API: " + Property.API_FRONT);
			}
		}

		@Override
		protected void finished(Description description) {

			Log.info("[Finalizando o Teste: " + description.getMethodName() + "]");
		}
	};
}
