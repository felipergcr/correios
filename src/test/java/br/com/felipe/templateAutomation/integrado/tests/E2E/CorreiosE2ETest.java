package br.com.felipe.templateAutomation.integrado.tests.E2E;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import br.com.felipe.templateAutomation.config.Property;
import br.com.felipe.templateAutomation.config.Webdriver;
import br.com.felipe.templateAutomation.integrado.pages.HomePage;
import br.com.felipe.templateAutomation.integrado.tests.BaseTest;

public class CorreiosE2ETest extends BaseTest {
	
	private static Webdriver driver = new Webdriver();
	private static HomePage homePage = new HomePage(Webdriver.getDriver());
	
	@Before
	public void before() {
		driver.navegateTo(Property.API_FRONT);
	}
	
	@Test
	public void consultarPorCep() throws Throwable {
		
		String cep = "06454-050";
		String logradouro =  "Alameda Grajaú";
		String bairro =  "Alphaville Centro Industrial e Empresarial/Alphaville.";
		String localidade =  "Barueri/SP";
		
		homePage.aguardarBanner();
		
		homePage.inserirCepOuLogradouro(cep);
		
		driver.trocarAba(1);
		
		homePage.validarPesquisa(logradouro, bairro, localidade, cep);
		
		driver.fecharAba();
	}
	
	@Test
	public void consultarPorLogradouro() throws Throwable {
		
		String logradouro =  "Alameda Grajaú";
		
		homePage.aguardarBanner();
		
		homePage.inserirCepOuLogradouro(logradouro);
		
		driver.trocarAba(1);
		
		homePage.saidaPesquisa();
		
		driver.fecharAba();
	}
	
	@AfterClass
	public static void afterClass() {
		Webdriver.closeNavegator();
	}
}
