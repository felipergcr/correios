package br.com.felipe.templateAutomation.integrado.tests.E2E;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.felipe.templateAutomation.integrado.tests.BaseTest;

@RunWith(Suite.class)
@Suite.SuiteClasses(
		
		{ 	
			CorreiosE2ETest.class,
		})

public class SuitesE2ETest extends BaseTest {
}
