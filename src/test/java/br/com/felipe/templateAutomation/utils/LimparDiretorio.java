package br.com.felipe.templateAutomation.utils;

import java.io.File;

public class LimparDiretorio {

	private static void remover(File f) {
		if (f.isDirectory()) {
			File[] files = f.listFiles();
			for (int i = 0; i < files.length; ++i) {
				remover(files[i]);
			}
		}
		f.delete();
	}

	public static void limparDiretorio(String path) {
		remover(new File(path));

		File diretorio = new File(path);
		diretorio.mkdir();
	}
}
